// класс Machine
class Machine {
    constructor () {
        this.isTurn = false;
    }

    turnOn () {
        this.isTurn = true;
        console.log('Вкл');
    }
    turnOff () {
        this.isTurn = false;
        console.log('Выкл');
    }
}

// класс HomeAppliance
class HomeAppliance extends Machine {
    constructor () {
        super();
        this.isPlugIn = false;
    }

    plugIn () {
        this.isPlugIn = true;
        console.log('Подключен к сети!');
    };
    plugOff () {
        this.isPlugIn = false;
        console.log('Не подключен к сети!');
    }
}

// класс WashingMachine
class WashingMachine extends HomeAppliance {
    constructor () {
        super();
    }
    turnOn () {
        if (this.isPlugIn) {
            this.isTurn = true;
            console.log('Проверку прошла!');
        }
        else {
            console.log('Не прошла проверку!');
        }
    }

    run () {
        if (this.isTurn && this.isPlugIn) {
            console.log('Стиральная машина начала работу!');
        }
        else {
            console.log('Стиральная машина не начала работу!');
        }
    }
}

// класс LightSource
class LightSource extends HomeAppliance {

    constructor () {
        super();
        this.setLevelAmaunt = 0;
    }
    setLevel (amount) {
        if (amount >= 1 && amount <= 100) {
            this.setLevelAmaunt = amount;
            console.log(`Уровень освещения ${this.setLevelAmaunt}!`);
        }
        else {
            console.log(
                'Ошибка! Задайте уровень освещения в интервале от 1 до 100!'
            );
        }
    }
}


// класс AutoVehicle
class AutoVehicle extends Machine {
    constructor() {
        super();
        this.x = 0;
        this.y = 0;
    }

    setPosition (x, y) {
        this.x = x;
        this.y = y;
    }
}

// создаем класс Car
class Car extends AutoVehicle {
    constructor() {
        super();
        self = this;
        this.speed = 10;
    }

    setSpeed (speed) {
        this.speed = speed;
        console.log(`Машина движется со скоростю = ${speed} км/час!`);
    }
    run (x, y) {
        const interval = setInterval(() => {
            let newX = self.x += self.speed;
            let newY = self.y += self.speed;
            if (newX > x) newX = x;
            if (newY > y) newY = y;
            self.setPosition(newX, newY);
            console.log(`Новые координаты = ${newX}`, newY);
            if (newX === x && newY === y) clearInterval(interval);
        }, 1000);
    }
}